package pacote;

public class BeanCadastroLivro{

    private String titulo, autor, genero, editora, idioma, descricao;
    private int paginas, edicao, ano;
    
    private byte[] imagem;
    private int id;
    private int idUsuario;

    public int getId() {
        return id;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public BeanCadastroLivro() {
    }

    public BeanCadastroLivro(String titulo, String autor, String genero, String editora, String idioma, String descricao, int paginas, int edicao, int ano, byte[] imagem) {
        this.titulo = titulo;
        this.autor = autor;
        this.genero = genero;
        this.editora = editora;
        this.idioma = idioma;
        this.descricao = descricao;
        this.paginas = paginas;
        this.edicao = edicao;
        this.ano = ano;
        this.imagem = imagem;
    }


    
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String título) {
        this.titulo = título;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getPaginas() {
        return paginas;
    }

    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }

    public int getEdicao() {
        return edicao;
    }

    public void setEdicao(int edicao) {
        this.edicao = edicao;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public byte[] getImagem() {
        return imagem;
    }

    public void setImagem(byte[] imagem) {
        this.imagem = imagem;
    }

    void setId(int aInt) {
        this.id = aInt;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
}
        

