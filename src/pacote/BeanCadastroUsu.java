package pacote;

import java.util.ArrayList;
import java.util.List;

public class BeanCadastroUsu {

    private String nomeComp, nomeUsu, dataNasci, CPF, senha, confSenha, email, telefone, celular, sexo;
    int id;
    ArrayList<Integer> generos;

    public BeanCadastroUsu() {
        this.generos = new ArrayList<>();
    }
    
    public ArrayList<Integer> getGeneros(){
        return this.generos;
    }

    public void adicionaGenero(int codigo) {
        if (this.generos.indexOf(id) == -1)
            this.generos.add(codigo);
    }

    public void limpaGeneros() {
        this.generos.clear();
    }
    
    public boolean generoExiste(int id) {
        return this.generos.indexOf(id) > -1;
    }

    public BeanCadastroUsu(String nomeComp, String nomeUsu, String dataNasci, String CPF, String senha, String confSenha, String email, String telefone, String celular, String sexo, int id) {
        this.nomeComp = nomeComp;
        this.nomeUsu = nomeUsu;
        this.dataNasci = dataNasci;
        this.CPF = CPF;
        this.senha = senha;
        this.confSenha = confSenha;
        this.email = email;
        this.telefone = telefone;
        this.celular = celular;
        this.sexo = sexo;
        this.id = id;
    }

    public String getNomeComp() {
        return nomeComp;
    }

    public void setNomeComp(String nomeComp) {
        this.nomeComp = nomeComp;
    }

    public String getNomeUsu() {
        return nomeUsu;
    }

    public void setNomeUsu(String nomeUsu) {
        this.nomeUsu = nomeUsu;
    }

    public String getDataNasci() {
        return dataNasci;
    }

    public void setDataNasci(String dataNasci) {
        this.dataNasci = dataNasci;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getConfSenha() {
        return confSenha;
    }

    public void setConfSenha(String confSenha) {
        this.confSenha = confSenha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
