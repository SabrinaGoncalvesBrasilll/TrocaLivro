package pacote;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class DaoCadastraLivro {

    public boolean insert(BeanCadastroLivro beanLivro){
        
        // usando  java.sql.jdbc.Driver para obter a conxão
        String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        String DB_URL = "jdbc:mysql://localhost/tcc";
                 
        // login e senha do PC
        String USER = "root";
        String PASS = "info2015";
               
        Connection conn = null;
        //Statement stmt = null;
        boolean funcionou = false;
        
        try {
            Class.forName(JDBC_DRIVER);
            //stmt = conn.createStatement();
            String sql = "insert into livro "+
                         "(idUsuario_FK, titulo, autor, genero, paginas, editora, edicao, ano, idioma, descricao) " +
                         "values"+
                         "(?, ?,?,?,?,?,?,?,?,?)";
            //String SQL = "insert into usuario ( titulo, autor, genero, paginas, editora, edicao, ano, idioma, descrica) values (?,?,?,?,?,?,?,?,?)";
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, beanLivro.getIdUsuario());
            stmt.setString(2, beanLivro.getTitulo());
            stmt.setString(3, beanLivro.getAutor());
            stmt.setString(4, beanLivro.getGenero());
            stmt.setInt(5, beanLivro.getPaginas());
            stmt.setString(6, beanLivro.getEditora());
            stmt.setInt(7, beanLivro.getEdicao());
            stmt.setInt(8, beanLivro.getAno());
            stmt.setString(9, beanLivro.getIdioma());
            stmt.setString(10, beanLivro.getDescricao());
            stmt.executeUpdate();
            
//            ResultSet rs = stmt.getGeneratedKeys();
//            if (rs.next()) {
//                // por que pegar o id 
//                beanLivro.setId(rs.getInt(1));
//            }
            stmt.close();
            conn.close();
            funcionou = true;
        } catch (Exception a) {
            System.out.println(a);
        }
        return funcionou;        
    }
    
    public ArrayList<BeanCadastroLivro> select(){
        
        String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        String DB_URL = "jdbc:mysql://localhost/TCC";

        String USER = "root";
        String PASS = "info2015";

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        ArrayList<BeanCadastroLivro> informações = new ArrayList<BeanCadastroLivro>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select * from livro;");
            
            while(rs.next()) {
            } 
        } catch (Exception a) {
            System.out.println(a);
        }
        return informações;
       
    }   
}
