package pacote;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DaoCadastroUsu {

    String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    String DB_URL = "jdbc:mysql://localhost/tcc";

    String USER = "root";
    String PASS = "info2015";

    public DaoCadastroUsu() {
    }

    public boolean insert(BeanCadastroUsu bean) {

        System.out.println("Usuario:" + bean.getNomeUsu() + ":");
        System.out.println("Senha:" + bean.getSenha() + ":");
        System.out.println("sexo:" + bean.getSexo() + ":");
        boolean funcionou = false;
        Connection conn = null;
        try {
            Class.forName(JDBC_DRIVER);

            String SQL = "insert into usuario ( Nomeusu, Senha, email, Telefone, celular, sexo, cpf, NomeComp) values (?,?,?,?,?,?,?,?)";

            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            PreparedStatement stmt = conn.prepareStatement(SQL);
            stmt.setString(1, bean.getNomeUsu());
            stmt.setString(2, bean.getSenha());
            stmt.setString(3, bean.getEmail());
            stmt.setString(4, bean.getTelefone());
            stmt.setString(5, bean.getCelular());
            stmt.setString(6, bean.getSexo());
            stmt.setString(7, bean.getCPF());
            stmt.setString(8, bean.getNomeComp());
            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                bean.setId(rs.getInt(1));
            }
            
            atualizaGenerosUsuario(bean);
            
            stmt.close();
            conn.close();
            funcionou = true;
        } catch (Exception a) {
            System.out.println(a);
        }
        return funcionou;
    }

    public ArrayList<BeanCadastroUsu> select() {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        ArrayList<BeanCadastroUsu> informações = new ArrayList<BeanCadastroUsu>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select * from usuario;");

            while (rs.next()) {

            }
        } catch (Exception a) {
            System.out.println(a);
        }
        return informações;
    }

    public void montaListaGeneros(BeanCadastroUsu cadastroUsu) {
        // monta a lista de gêneros do usuário com base nos gêneros
        // que ele escolheu como favoritos...
        try{
            
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);        
        PreparedStatement stmt = conn.prepareStatement("select idGenero from usuario_genero where idUsuario = ?");
        stmt.setInt(1, cadastroUsu.getId());
        ResultSet rs = stmt.executeQuery();
        cadastroUsu.limpaGeneros();
        while (rs.next()) {
            cadastroUsu.adicionaGenero(rs.getInt("idGenero"));
        }
        stmt.close();
        conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void atualizaGenerosUsuario(BeanCadastroUsu cadastroUsu) throws SQLException, ClassNotFoundException {
        
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            
            // apaga todos os gêneros que o usuário possa ter escolhido....
            PreparedStatement stmt = conn.prepareStatement("delete from usuario_genero where idUsuario = ?");
            stmt.setInt(1, cadastroUsu.getId());
            stmt.execute();
            
            
            PreparedStatement insert = conn.prepareStatement("insert into usuario_genero (idUsuario, idGenero) values (?, ?);");
            insert.setInt(1, cadastroUsu.getId());
            // insere somente os gêneros que foram marcados como favoritos e adicionados
            // na lista de gêneros do beanUsuario
            ArrayList<Integer> lista = cadastroUsu.getGeneros();
            for (Integer idGenero : lista) {
                insert.setInt(2, idGenero);
                insert.execute();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public boolean loginExiste(String nomeUsuario, String senhaUsuario, BeanCadastroUsu cadastroUsu) {
        boolean achou = false;
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            PreparedStatement stmt = conn.prepareStatement("select * from usuario where nomeusu = ? and senha = ?");
            stmt.setString(1, nomeUsuario);
            stmt.setString(2, senhaUsuario);

            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                achou = true;
                cadastroUsu.setId(rs.getInt("idUsuario"));
                cadastroUsu.setNomeUsu(rs.getString("nomeusu"));
                cadastroUsu.setEmail(rs.getString("email"));
            }
            stmt.close();

            montaListaGeneros(cadastroUsu);
            conn.close();
        } catch (Exception a) {
            System.out.println(a);
        }
        return achou;
    }

    
    public void carregaDadosUsuario(int idUsuario, BeanCadastroUsu beanUsuario) {
        // 
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            PreparedStatement stmt = conn.prepareStatement("select * from usuario where idUsuario = ?");
            stmt.setInt(1, idUsuario);

            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                beanUsuario.setNomeComp(rs.getString("NomeComp"));
                System.out.println("NomeUsu:" + rs.getString("NomeUsu"));
                beanUsuario.setNomeUsu(rs.getString("NomeUsu")); 
                beanUsuario.setSenha(rs.getString("Senha"));
                beanUsuario.setTelefone(rs.getString("Telefone"));
                beanUsuario.setCelular(rs.getString("Celular"));
                beanUsuario.setSexo(rs.getString("Sexo"));
                beanUsuario.setEmail(rs.getString("email"));
                beanUsuario.setCPF(rs.getString("cpf"));
            }
            montaListaGeneros(beanUsuario);
            stmt.close();
            conn.close();
        } catch (Exception a) {
            System.out.println(a);
            
            
            
        }
        
    }
    
    
    public boolean CadastroExiste(BeanCadastroUsu beanUsuario) {

        boolean achou = false;
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //nomeComp, nomeUsu, dataNasci, CPF, senha, confSenha, email, telefone, celular, sexo
            PreparedStatement stmt = conn.prepareStatement("select * from usuario where nomeusu = ? or cpf = ? or email = ?");
            stmt.setString(1, beanUsuario.getNomeUsu());
            stmt.setString(2, beanUsuario.getCPF());
            stmt.setString(3, beanUsuario.getEmail());

            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                achou = true;
            }
            stmt.close();
            conn.close();
        } catch (Exception a) {
            System.out.println(a);
        }
        return achou;
    }

    public void Altera(BeanCadastroUsu bean) throws SQLException, ClassNotFoundException {
        
        Connection conn = null;
        PreparedStatement stmt2 = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            String sql = "UPDATE usuario SET   Senha = ?, email = ?, Telefone = ?, celular = ?, sexo = ?, WHERE idUsuario = " + bean.getId();
            stmt2 = conn.prepareStatement(sql);
            stmt2.setString(1, bean.getSenha());
            stmt2.setString(2, bean.getEmail());
            stmt2.setString(3, bean.getTelefone());
            stmt2.setString(4, bean.getCelular());
            stmt2.setString(5, bean.getSexo());
            stmt2.execute();
            stmt2.close();
            conn.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public void AlteraGeneros(BeanCadastroUsu bean) throws SQLException, ClassNotFoundException {
        
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
          
            String sql = "Delete * from usuario_genero where idUsuario = " + bean.getId();
            String inserir = "insert into usuario_genero(idUsuario, idGenero) values (?, ?)";
            stmt = conn.prepareStatement(sql);
            stmt = conn.prepareStatement(inserir);
            stmt.setInt(1, bean.getId());
            stmt.execute();
            
            ArrayList<Integer> lista = bean.getGeneros();
          for (Integer idGenero : lista) {
                stmt.setInt(2, idGenero);
                stmt.execute();
            }
                       
        } catch (Exception e) {
          System.out.println(e);
        }
    }

    public static void main(String[] args) {
        DaoCadastroUsu dao = new DaoCadastroUsu();
        //String nomeComp, String nomeUsu, String dataNasci, String CPF, String senha,String email, String telefone, String celular, String sexo, 
        //dao.insert(new BeanCadastroUsu( "teste teste4","teste4" , "12/12/2012" , "12345678996", "testeteste4", "teste.teste4@gmail.com", "12345698", "96325874", "Feminino"));
    }
}
