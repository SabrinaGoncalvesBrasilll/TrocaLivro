--drop database if exists TCC;
/*
create database TCC;

use TCC;

Create table usuario(
    idUsuario int auto_increment not null,
    NomeComp varchar(20) not null,
    NomeUsu varchar(20) not null,
    Senha varchar(20) not null,
    Telefone varchar(20) not null,
    Celular varchar(20) not null,
    Sexo varchar(15),
    email varchar(40) not null,
    cpf varchar(20),
    primary key (idUsuario)
);

Create table livro(
    idLivro int auto_increment not null,
    idUsuario_FK int not null,
    titulo varchar(100),
    autor varchar(40),
    genero varchar(40),
    paginas int not null,
    editora varchar(40),
    edicao int not null,
    ano int not null,
    idioma varchar(40),
    descricao varchar(300),
    primary key (idLivro),
    foreign key (idUsuario_FK) references Usuario (idUsuario)
);


//alter table usuario add constraint usuario_pk primary key (idUsuario);


Create table generos(
idGenero int not null,
nome varchar (40),
primary key (idGenero)
);

Create table usuario_genero(
idUsuario int not null,
idGenero int not null,
primary key (idUsuario, idGenero),
foreign key (idUsuario) references Usuario (idUsuario),
foreign key (idGenero) references Generos (idGenero)
);


insert into usuario(NomeComp, NomeUsu, Senha, Telefone, Celular, email) values 
("admin", "admin", "admin", "admin", "admin", "admin");

insert into Generos (idGenero, nome) values (1, "Ação");
insert into Generos (idGenero, nome) values (2, "Romance");
insert into Generos (idGenero, nome) values (3, "Ficção Científica");
insert into Generos (idGenero, nome) values (4, "Comédia");
insert into Generos (idGenero, nome) values (5, "Aventura");
insert into Generos (idGenero, nome) values (6, "Suspense");
insert into Generos (idGenero, nome) values (7, "Terror");
insert into Generos (idGenero, nome) values (8, "Conto de Fadas");
insert into Generos (idGenero, nome) values (9, "Técnico");
insert into Generos (idGenero, nome) values (10, "Distopia");

/*-- criar tabela de gênero com PK
create table genero (
id ) OK

-- inserir os gêneros padrões
insert into genero (id, nome) values (1, "Ação"); OK

-- criar a FK na tabela de livros para a tabela de usuários OK

-- criar a tabela usuario_genero, com PK e FK para tabelas PAIS OK

select * from TCC.livro;
--select * from TCC.usuario;

select idGenero from generos where idUsuario = ?;

delete from generos where idUsuario = ?;

insert into generos (idUsuario, idGenero) values (?, ?)
*/

select * from tcc.usuario_genero;
