package tcc;

import com.sun.org.apache.xml.internal.serializer.utils.Utils;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pacote.BeanCadastroUsu;
import pacote.DaoCadastroUsu;

public class CadastroDeUsuarioJFrame extends javax.swing.JFrame {

    private boolean alterando = false;

    public void ativaModoAlteracao() {
        this.alterando = true;
    }
  
    public CadastroDeUsuarioJFrame() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        label1 = new java.awt.Label();
        label2 = new java.awt.Label();
        label3 = new java.awt.Label();
        label4 = new java.awt.Label();
        label5 = new java.awt.Label();
        label6 = new java.awt.Label();
        label7 = new java.awt.Label();
        label8 = new java.awt.Label();
        label9 = new java.awt.Label();
        label10 = new java.awt.Label();
        label11 = new java.awt.Label();
        edTelef = new java.awt.TextField();
        edTelefCel = new java.awt.TextField();
        edCPF = new java.awt.TextField();
        eddataNasci = new java.awt.TextField();
        edEmail = new java.awt.TextField();
        edSexo = new javax.swing.JComboBox();
        edNomeComp = new java.awt.TextField();
        edNomeUsu = new java.awt.TextField();
        jButton1 = new javax.swing.JButton();
        edSenha = new javax.swing.JPasswordField();
        edConfSenha = new javax.swing.JPasswordField();
        label12 = new java.awt.Label();
        radioAcao = new javax.swing.JRadioButton();
        radioRomance = new javax.swing.JRadioButton();
        radioFiccao = new javax.swing.JRadioButton();
        radioComedia = new javax.swing.JRadioButton();
        radioAventura = new javax.swing.JRadioButton();
        radioSuspense = new javax.swing.JRadioButton();
        radioTerror = new javax.swing.JRadioButton();
        radioContos = new javax.swing.JRadioButton();
        radioTecnico = new javax.swing.JRadioButton();
        radioDistopia = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(204, 255, 204));

        label1.setFont(new java.awt.Font("Comic Sans MS", 0, 60)); // NOI18N
        label1.setText("Cadastro de Usuários:");

        label2.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        label2.setText("Nome completo*:");

        label3.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        label3.setText("Nome de usuário*:");

        label4.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        label4.setText("CPF:");

        label5.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        label5.setText("Data de Nascimento:");

        label6.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        label6.setText("Sexo:");

        label7.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        label7.setText("Telefone:");

        label8.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        label8.setText("Telefone celular:");

        label9.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        label9.setText("E-mail:");

        label10.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        label10.setText("Senha:");

        label11.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        label11.setText("Confirme a Senha:");

        edTelef.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        edTelefCel.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        edCPF.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        edCPF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edCPFActionPerformed(evt);
            }
        });

        eddataNasci.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        eddataNasci.setName(""); // NOI18N
        eddataNasci.setText("Mudar ordem");

        edEmail.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        edSexo.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        edSexo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecione", "Feminino", "Masculino", "Outro" }));
        edSexo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edSexoActionPerformed(evt);
            }
        });

        edNomeComp.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        edNomeComp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edNomeCompActionPerformed(evt);
            }
        });

        edNomeUsu.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N

        jButton1.setText("Próximo");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        label12.setFont(new java.awt.Font("Comic Sans MS", 0, 30)); // NOI18N
        label12.setText("Gêneros:");

        radioAcao.setBackground(new java.awt.Color(204, 255, 204));
        radioAcao.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        radioAcao.setText("Ação");
        radioAcao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioAcaoActionPerformed(evt);
            }
        });

        radioRomance.setBackground(new java.awt.Color(204, 255, 204));
        radioRomance.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        radioRomance.setText("Romance");

        radioFiccao.setBackground(new java.awt.Color(204, 255, 204));
        radioFiccao.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        radioFiccao.setText(" Ficção científica");

        radioComedia.setBackground(new java.awt.Color(204, 255, 204));
        radioComedia.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        radioComedia.setText("Comédia");
        radioComedia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioComediaActionPerformed(evt);
            }
        });

        radioAventura.setBackground(new java.awt.Color(204, 255, 204));
        radioAventura.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        radioAventura.setText("Aventura");

        radioSuspense.setBackground(new java.awt.Color(204, 255, 204));
        radioSuspense.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        radioSuspense.setText("Suspense");

        radioTerror.setBackground(new java.awt.Color(204, 255, 204));
        radioTerror.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        radioTerror.setText("Terror");

        radioContos.setBackground(new java.awt.Color(204, 255, 204));
        radioContos.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        radioContos.setText("Conto de Fadas");

        radioTecnico.setBackground(new java.awt.Color(204, 255, 204));
        radioTecnico.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        radioTecnico.setText("Técnico");

        radioDistopia.setBackground(new java.awt.Color(204, 255, 204));
        radioDistopia.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        radioDistopia.setText("Distopia");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(90, 90, 90)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(label12, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(185, 185, 185)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(label6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(label5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(label3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(label2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(label4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(edCPF, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(eddataNasci, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(edSexo, 0, 140, Short.MAX_VALUE)
                                    .addComponent(edNomeComp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(edNomeUsu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(186, 186, 186)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(label7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(label8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(label9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(label11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(label10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(edTelef, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                                    .addComponent(edTelefCel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(edEmail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(edSenha)
                                    .addComponent(edConfSenha))))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(340, 340, 340)
                            .addComponent(jButton1)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(182, 182, 182)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(radioFiccao, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(radioComedia, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(radioContos, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(radioTerror, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(radioAcao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(radioRomance))
                                .addGap(67, 67, 67)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(radioSuspense, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(radioAventura, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(radioDistopia, javax.swing.GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
                                    .addComponent(radioTecnico, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                .addContainerGap(170, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(label7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(edTelef, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(30, 30, 30))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(edNomeComp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(label2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(38, 38, 38)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(edTelefCel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(label8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(label3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(edNomeUsu, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(label4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(edEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(edCPF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(eddataNasci, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(label5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(edSenha)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(label9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(label10, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(label6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(label11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(edSexo)
                    .addComponent(edConfSenha, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(label12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(171, 171, 171)
                        .addComponent(jButton1)
                        .addGap(32, 32, 32))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(radioAcao)
                            .addComponent(radioAventura)
                            .addComponent(radioTecnico, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 3, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(radioRomance)
                            .addComponent(radioSuspense)
                            .addComponent(radioDistopia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(radioFiccao)
                            .addComponent(radioTerror, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(radioComedia)
                            .addComponent(radioContos))
                        .addGap(125, 125, 125))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        //jRadioButton1.isSelected()
        String NomeComp = edNomeComp.getText().trim();
        String Usu = edNomeUsu.getText().trim();
        String DataNasci = eddataNasci.getText().trim();
        String cpf = edCPF.getText().trim();
        String Sexo = edSexo.getSelectedItem().toString();
        String Telefone = edTelef.getText().trim();
        String Celular = edTelefCel.getText().trim();
        String Email = edEmail.getText().trim();
        String Senha = "";
        String ConfSenha = "";

        char[] letras = edSenha.getPassword();
        char[] letrasConf = edConfSenha.getPassword();

        for (int i = 0; i < letras.length; i++) {
            Senha = Senha + letras[i];
        }

        for (int i = 0; i < letrasConf.length; i++) {
            ConfSenha = ConfSenha + letrasConf[i];
        }
        System.out.println("Senha:" + Senha + ":");
        System.out.println("ConfSenha:" + ConfSenha + ":");
        if (NomeComp.isEmpty() && NomeComp.length() < 10) {
            Mensagem.msgErro("O nome completo deve ser informado e ter mais de 10 números..");
            edNomeComp.requestFocus();
        } else if (Usu.isEmpty() && Usu.length() < 6) {
            Mensagem.msgErro("O nome de usuário deve ser informado e ter mais de 6 números..");
            edNomeUsu.requestFocus();
        } else if (cpf.isEmpty() && cpf.length() < 11) {
            Mensagem.msgErro("O CPF deve ser informado e ter mais de 11 números.");
            edCPF.requestFocus();
        } else if (Email.isEmpty()) {
            Mensagem.msgErro("O E-mail deve ser informado.");
            edEmail.requestFocus();
        } else if (Senha.isEmpty() && Senha.length() < 6) {
            Mensagem.msgErro("A senha deve ser informada e ter mais de 6 números.");
            edSenha.requestFocus();
        } else if (ConfSenha.isEmpty() && ConfSenha.length() < 6) {
            Mensagem.msgErro("A confirmação da senha deve ser informada e ter mais de 6 números.");
            edConfSenha.requestFocus();
        } else if (!Senha.equals(ConfSenha)) {
            Mensagem.msgErro("A confirmação da senha deve ser igual a senha.");
            edConfSenha.requestFocus();
        } else {
            BeanCadastroUsu bean = new BeanCadastroUsu();
            bean.setNomeComp(NomeComp);
            bean.setNomeUsu(Usu);
            bean.setDataNasci(DataNasci);
            bean.setCPF(cpf);
            bean.setSexo(Sexo);
            bean.setTelefone(Telefone);
            bean.setCelular(Celular);
            bean.setEmail(Email);
            bean.setSenha(Senha);
            bean.setConfSenha(ConfSenha);
            if (radioAcao.isSelected()){ 
                bean.adicionaGenero(Util.idGeneroAcao);
            }
            if (radioRomance.isSelected()){ 
                bean.adicionaGenero(Util.idGeneroRomance);
            }
            if (radioFiccao.isSelected()){ 
                bean.adicionaGenero(Util.idGeneroFiccao);
            }
            if (radioComedia.isSelected()){ 
                bean.adicionaGenero(Util.idGeneroComedia);
            }
            if (radioAventura.isSelected()){ 
                bean.adicionaGenero(Util.idGeneroAventura);
            }
            if (radioSuspense.isSelected()){ 
                bean.adicionaGenero(Util.idGeneroSuspense);
            }
            if (radioTerror.isSelected()){ 
                bean.adicionaGenero(Util.idGeneroTerror);
            }
            if (radioContos.isSelected()){ 
                bean.adicionaGenero(Util.idGeneroContos);
            }
            if (radioDistopia.isSelected()){ 
                bean.adicionaGenero(Util.idGeneroDistopia);
            }
            if (radioTecnico.isSelected()){ 
                bean.adicionaGenero(Util.idGeneroTecnico);
            }
            
            DaoCadastroUsu dao = new DaoCadastroUsu();
            if (this.alterando) {
                // verificar se existe OUTRO usuário
                // com o mesmo endereço de e-mail que foi informado
                // pois o usuário pode alterar o e-mail
                // mas este dado é único em toda a tabela
                // de usuário
            } else {
                if (dao.CadastroExiste(bean)) {
                    Mensagem.msgErro("Os dados de e-mail, Nome de Usuário ou CPF já existem para outro usuário.");
                } else {
                    if (dao.insert(bean)) {
                        Mensagem.msgInformacao("Cadastro realizado com sucesso.");
                        this.setVisible(false);
                        PerfilJFrame Perfil = new PerfilJFrame();
                        Perfil.setVisible(true);
                    } else {
                        Mensagem.msgErro("Ocorreu um erro durante a inclusão do usuário. Contate o suporte técnico (Sabrina e Pâmela).");
                    }
                }
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void edNomeCompActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edNomeCompActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edNomeCompActionPerformed

    private void edSexoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edSexoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edSexoActionPerformed

    private void edCPFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edCPFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edCPFActionPerformed

    private void radioAcaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioAcaoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_radioAcaoActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        if (this.alterando) {
            // no modo de alteração é necessário
            // fazer um select na tabela de usuário para
            // trazer os dados do banco e colocar os dados na tela
            // também precisam mostrar (select) quais são
            // os gêneros que o usuário já havia selecionado
            BeanCadastroUsu usuario = new BeanCadastroUsu();
            usuario.setId(Util.getId());
            
            DaoCadastroUsu dao = new DaoCadastroUsu();
            try {
                dao.carregaDadosUsuario(Util.getId(), usuario);
                edNomeComp.setText(usuario.getNomeComp());
                edNomeUsu.setText(usuario.getNomeUsu());
                edSenha.setText(usuario.getSenha());
                edTelef.setText(usuario.getTelefone());
                edTelefCel.setText(usuario.getCelular());
                edSexo.setSelectedItem(usuario.getSexo());
                edEmail.setText(usuario.getEmail());
                edCPF.setText(usuario.getCPF());
                edCPF.setEnabled(false);

                radioAcao.setSelected(usuario.generoExiste(Util.idGeneroAcao));
                radioAventura.setSelected(usuario.generoExiste(Util.idGeneroAventura));
                radioTecnico.setSelected(usuario.generoExiste(Util.idGeneroTecnico));
                radioRomance.setSelected(usuario.generoExiste(Util.idGeneroRomance));
                radioComedia.setSelected(usuario.generoExiste(Util.idGeneroComedia));
                radioFiccao.setSelected(usuario.generoExiste(Util.idGeneroFiccao));
                radioTerror.setSelected(usuario.generoExiste(Util.idGeneroTerror));
                radioSuspense.setSelected(usuario.generoExiste(Util.idGeneroSuspense));
                radioContos.setSelected(usuario.generoExiste(Util.idGeneroContos));
                radioDistopia.setSelected(usuario.generoExiste(Util.idGeneroDistopia));
                
                //jRadioButton1.setSelected(false);
                // desabilitar os campos da tela que são únicos
                // (nome completo, nome de usuário, entre outros)
                // para evitar que o usuário possa alterá-los
                // e duplicar com os dados de outro usuário
                // edNomeComp.setEnabled(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_formWindowOpened

    private void radioComediaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioComediaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_radioComediaActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CadastroDeUsuarioJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CadastroDeUsuarioJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CadastroDeUsuarioJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CadastroDeUsuarioJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CadastroDeUsuarioJFrame().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.TextField edCPF;
    private javax.swing.JPasswordField edConfSenha;
    private java.awt.TextField edEmail;
    private java.awt.TextField edNomeComp;
    private java.awt.TextField edNomeUsu;
    private javax.swing.JPasswordField edSenha;
    private javax.swing.JComboBox edSexo;
    private java.awt.TextField edTelef;
    private java.awt.TextField edTelefCel;
    private java.awt.TextField eddataNasci;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private java.awt.Label label1;
    private java.awt.Label label10;
    private java.awt.Label label11;
    private java.awt.Label label12;
    private java.awt.Label label2;
    private java.awt.Label label3;
    private java.awt.Label label4;
    private java.awt.Label label5;
    private java.awt.Label label6;
    private java.awt.Label label7;
    private java.awt.Label label8;
    private java.awt.Label label9;
    private javax.swing.JRadioButton radioAcao;
    private javax.swing.JRadioButton radioAventura;
    private javax.swing.JRadioButton radioComedia;
    private javax.swing.JRadioButton radioContos;
    private javax.swing.JRadioButton radioDistopia;
    private javax.swing.JRadioButton radioFiccao;
    private javax.swing.JRadioButton radioRomance;
    private javax.swing.JRadioButton radioSuspense;
    private javax.swing.JRadioButton radioTecnico;
    private javax.swing.JRadioButton radioTerror;
    // End of variables declaration//GEN-END:variables
}
