package tcc;

import javax.swing.JOptionPane;

public class Mensagem {

    static void msgInformacao(String mensagem) {
        JOptionPane.showMessageDialog(null, mensagem, "Informação", JOptionPane.INFORMATION_MESSAGE);
    }
    static void msgErro(String mensagem) {
            //JOptionPane.QUESTION_MESSAGE
            //JOptionPane.WARNING_MESSAGE
            //JOptionPane.INFORMATION_MESSAGE
        JOptionPane.showMessageDialog(null, mensagem, "Erro", JOptionPane.ERROR_MESSAGE);
    }

}
