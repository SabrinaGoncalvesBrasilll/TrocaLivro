/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcc;

public class Util {
    
    private static int id;
    
    public static final int idGeneroAcao = 1;
    public static final int idGeneroRomance = 2;
    public static final int idGeneroFiccao = 3;
    public static final int idGeneroComedia = 4;
    public static final int idGeneroAventura = 5;
    public static final int idGeneroSuspense = 6;
    public static final int idGeneroTerror = 7;
    public static final int idGeneroContos = 8;
    public static final int idGeneroTecnico = 9;
    public static final int idGeneroDistopia = 10;

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        Util.id = id;
    }
}
